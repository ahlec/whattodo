﻿using System;
using System.Windows.Input;

namespace WhatToDo
{
	class BasicCommand : ICommand
	{
		public BasicCommand( Action<Object> function )
		{
			_function = function;
		}

		public Boolean CanExecute( Object parameter )
		{
			return true;
		}

		public void Execute(Object parameter)
		{
			_function( parameter );
		}

#pragma warning disable 0067
		public event EventHandler CanExecuteChanged;
#pragma warning restore 0067

		readonly Action<Object> _function;
	}
}
