﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace WhatToDo
{
	class ManageDataDialogViewModel : DependencyObject
	{
		public ManageDataDialogViewModel()
		{
			Suggestions = SuggestionManager.GetAllSuggestions();
		}

		public ObservableCollection<Suggestion> Suggestions { get; set; }
		public Suggestion SelectedSuggestion
		{
			get { return (Suggestion)GetValue(SelectedSuggestionProperty); }
			set { SetValue(SelectedSuggestionProperty, value); }
		}

		void AddSuggestion(Object o)
		{
			Suggestion added = new Suggestion();

			Int32 appendedIndex = 1;
			String currentNewName = $"New Suggestion{appendedIndex}";
			while (Suggestions.Any(suggestion => suggestion.Text.Equals(currentNewName)))
			{
				++appendedIndex;
				currentNewName = $"New Suggestion{appendedIndex}";
			}

			added.Text = currentNewName;

			Suggestions.Add(added);
			SelectedSuggestion = added;
		}

		void RemoveSuggestion(Object o)
		{
			if (SelectedSuggestion == null)
			{
				return;
			}

			Suggestions.Remove(SelectedSuggestion);
			SelectedSuggestion = null;
		}

		void SaveSuggestions(Object o)
		{
			SuggestionManager.Save(Suggestions);
		}

		public ICommand AddSuggestionCommand => (_addSuggestionCommand ?? (_addSuggestionCommand = new BasicCommand(AddSuggestion)));
		public ICommand RemoveSuggestionCommand => (_removeSuggestionCommand ?? (_removeSuggestionCommand = new BasicCommand(RemoveSuggestion)));
		public ICommand SaveSuggestionsCommand => (_saveSuggestionsCommand ?? (_saveSuggestionsCommand = new BasicCommand(SaveSuggestions)));

		BasicCommand _addSuggestionCommand;
		BasicCommand _removeSuggestionCommand;
		BasicCommand _saveSuggestionsCommand;

		public static readonly DependencyProperty SelectedSuggestionProperty = DependencyProperty.Register("SelectedSuggestion", typeof(Suggestion), typeof(ManageDataDialogViewModel));
	}
}
