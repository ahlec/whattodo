﻿using System;
using System.Xml.Linq;

namespace WhatToDo
{
	static class Versioning
	{
		public static Boolean UpdateSuggestionsXml(XElement xml)
		{
			Int32 xmlVersion = 0;
			if (xml.Attribute("version") != null)
			{
				xmlVersion = Int32.Parse(xml.Attribute("version").Value);
			}

			if (xmlVersion == Constants.XmlVersion)
			{
				return false;
			}

			for (Int32 updateVersion = xmlVersion; updateVersion < Constants.XmlVersion; ++updateVersion)
			{
				_updaters[updateVersion](xml);
			}

			xml.SetAttributeValue("version", Constants.XmlVersion);

			return true;
		}

		static void Update0To1(XElement xml)
		{
			// All this does is add the version attribute, which is done at the end of the UpdateSuggestionXml, so we don't need to do anything here
		}

		static Action<XElement>[] _updaters =
		{
			Update0To1
		};
	}
}
