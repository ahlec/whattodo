﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WhatToDo
{
	class SuggestionCriteria
	{
		public Boolean AreFiltersOn { get; set; }
		public Boolean IsFreeOnly { get; set; }
		public Boolean IsUnder21Only { get; set; }
		public Boolean IsOutdoorsOkay { get; set; }
		public Boolean IsHomeboundOnly { get; set; }

		public IEnumerable<Suggestion> SuggestionsAlreadyGiven { get; set; } = Enumerable.Empty<Suggestion>();
	}
}
