﻿using System;
using System.Xml.Linq;
using System.Windows;

namespace WhatToDo
{
	class Suggestion : DependencyObject
	{
		public Boolean IsFree
		{
			get { return (Boolean)GetValue(IsFreeProperty); }
			set { SetValue(IsFreeProperty, value); }
		}
		public Boolean IsUnder21
		{
			get { return (Boolean)GetValue(IsUnder21Property); }
			set { SetValue(IsUnder21Property, value); }
		}
		public Boolean IsOutdoors
		{
			get { return (Boolean)GetValue(IsOutdoorsProperty); }
			set { SetValue(IsOutdoorsProperty, value); }
		}
		public Boolean IsHomebound
		{
			get { return (Boolean)GetValue(IsHomeboundProperty); }
			set { SetValue(IsHomeboundProperty, value); }
		}
		public String Text
		{
			get { return (String)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public static Suggestion Parse(XElement xml)
		{
			return new Suggestion
			{
				IsFree = Boolean.Parse(xml.Attribute("IsFree").Value),
				IsUnder21 = Boolean.Parse(xml.Attribute("IsUnder21").Value),
				IsOutdoors = Boolean.Parse(xml.Attribute("IsOutdoors").Value),
				IsHomebound = Boolean.Parse(xml.Attribute("IsHomebound").Value),
				Text = xml.Value
			};
		}

		public XElement ToXml()
		{
			XElement xml = new XElement("suggestion");
			xml.SetAttributeValue("IsFree", IsFree.ToString().ToLower());
			xml.SetAttributeValue("IsUnder21", IsUnder21.ToString().ToLower());
			xml.SetAttributeValue("IsOutdoors", IsOutdoors.ToString().ToLower());
			xml.SetAttributeValue("IsHomebound", IsHomebound.ToString().ToLower());
			xml.SetValue(Text);
			return xml;
		}

		public Suggestion Clone()
		{
			return new Suggestion
			{
				IsFree = IsFree,
				IsUnder21 = IsUnder21,
				IsOutdoors = IsOutdoors,
				IsHomebound = IsHomebound,
				Text = Text
			};
		}

		public Boolean MatchesCriteria(SuggestionCriteria criteria)
		{
			if (criteria == null || !criteria.AreFiltersOn)
			{
				return true;
			}

			if (criteria.IsFreeOnly && !IsFree)
			{
				return false;
			}

			if (criteria.IsUnder21Only && !IsUnder21)
			{
				return false;
			}

			if (!criteria.IsOutdoorsOkay && IsOutdoors)
			{
				return false;
			}

			if (criteria.IsHomeboundOnly && !IsHomebound)
			{
				return false;
			}

			return true;
		}

		public static readonly DependencyProperty IsFreeProperty = DependencyProperty.Register("IsFree", typeof(Boolean), typeof(Suggestion));
		public static readonly DependencyProperty IsUnder21Property = DependencyProperty.Register("IsUnder21", typeof(Boolean), typeof(Suggestion));
		public static readonly DependencyProperty IsOutdoorsProperty = DependencyProperty.Register("IsOutdoors", typeof(Boolean), typeof(Suggestion));
		public static readonly DependencyProperty IsHomeboundProperty = DependencyProperty.Register("IsHomebound", typeof(Boolean), typeof(Suggestion));
		public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String), typeof(Suggestion));
	}
}
