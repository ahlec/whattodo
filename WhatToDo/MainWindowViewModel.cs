﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace WhatToDo
{
	class MainWindowViewModel : DependencyObject
	{
		public MainWindowViewModel()
		{
			AreFiltersOn = false;
			UpdateDisplay(true);

			SuggestionManager.Saved += OnSuggestionManagerSaved;
		}

		void OnSuggestionManagerSaved(Object sender, EventArgs e)
		{
			_previousSuggestions.Clear();
			_nextSuggestions.Clear();
			_currentSuggestion = null;

			UpdateDisplay(true);
		}

		void ModifyData(Object parameter)
		{
			new ManageDataDialog
			{
				Owner = App.Current.MainWindow
			}.ShowDialog();
		}

		void ShowPreviousSuggestion(Object parameter)
		{
			if (_currentSuggestion == null || _previousSuggestions.Count == 0)
			{
				return;
			}

			_nextSuggestions.Insert(0, _currentSuggestion);
			_currentSuggestion = _previousSuggestions.Last();
			_previousSuggestions.RemoveAt(_previousSuggestions.Count - 1);
			UpdateDisplay();
		}

		void ShowNextSuggestion(Object parameter)
		{
			if (_nextSuggestions.Count > 0)
			{
				_previousSuggestions.Add(_currentSuggestion);
				_currentSuggestion = _nextSuggestions[0];
				_nextSuggestions.RemoveAt(0);
			}
			else
			{
				if (_currentSuggestion != null)
				{
					_previousSuggestions.Add(_currentSuggestion);
				}
				_currentSuggestion = SuggestionManager.GetNewSuggestion(GetCurrentCriteria());
			}

			UpdateDisplay();
		}

		static void OnCriteriaChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
		{
			MainWindowViewModel viewModel = ((MainWindowViewModel)o);

			if (viewModel._isInternallySyncingProperties)
			{
				return;
			}

			if (e.NewValue.Equals(true))
			{
				viewModel._isInternallySyncingProperties = true;
				if (e.Property == IsOutdoorsOkayProperty)
				{
					viewModel.IsHomeboundOnly = false;
				}
				else if (e.Property == IsHomeboundOnlyProperty)
				{
					viewModel.IsOutdoorsOkay = false;
				}
				viewModel._isInternallySyncingProperties = false;
			}

			viewModel.UpdateDisplay();
		}

		void UpdateDisplay(Boolean isInitializing = false)
		{
			SuggestionCriteria criteria = GetCurrentCriteria();

			if (_currentSuggestion == null)
			{
				HasCurrentSuggestion = false;
				if (!isInitializing)
				{
					return;
				}
				DoesCurrentSuggestionMatchCriteria = true;
			}
			else
			{
				HasCurrentSuggestion = true;
				CurrentSuggestionText = _currentSuggestion.Text;
				DoesCurrentSuggestionMatchCriteria = _currentSuggestion.MatchesCriteria(criteria);
			}

			HasPreviousSuggestions = _previousSuggestions.Count > 0;
			RemainingSuggestionsMatchingCriteria = SuggestionManager.GetRemainingSuggestionsMatchingCriteria(criteria);
			TotalRemainingSuggestions = SuggestionManager.GetRemainingSuggestionsTotal(criteria.SuggestionsAlreadyGiven);
			HasNextSuggestion = (RemainingSuggestionsMatchingCriteria > 0 || _nextSuggestions.Count > 0);
		}

		SuggestionCriteria GetCurrentCriteria()
		{
			return new SuggestionCriteria
			{
				AreFiltersOn = AreFiltersOn,
				IsFreeOnly = IsFree,
				IsUnder21Only = IsUnder21,
				IsOutdoorsOkay = IsOutdoorsOkay,
				IsHomeboundOnly = IsHomeboundOnly,
				SuggestionsAlreadyGiven = _previousSuggestions.Union(_nextSuggestions).Union(GetCurrentSuggestionAsEnumerable()).ToList()
			};
		}

		IEnumerable<Suggestion> GetCurrentSuggestionAsEnumerable()
		{
			if (_currentSuggestion == null)
			{
				yield break;
			}

			yield return _currentSuggestion;
		}

		public ICommand ModifyDataCommand => (_modifyDataCommand ?? (_modifyDataCommand = new BasicCommand(ModifyData)));
		public ICommand ShowPreviousSuggestionCommand => (_showPreviousSuggestionCommand ?? (_showPreviousSuggestionCommand = new BasicCommand(ShowPreviousSuggestion)));
		public ICommand ShowNextSuggestionCommand => (_showNextSuggestionCommand ?? (_showNextSuggestionCommand = new BasicCommand(ShowNextSuggestion)));

		public Boolean AreFiltersOn
		{
			get { return (Boolean)GetValue(AreFiltersOnProperty); }
			set { SetValue(AreFiltersOnProperty, value); }
		}
		public Boolean IsFree
		{
			get { return (Boolean)GetValue(IsFreeProperty); }
			set { SetValue(IsFreeProperty, value); }
		}
		public Boolean IsUnder21
		{
			get { return (Boolean)GetValue(IsUnder21Property); }
			set { SetValue(IsUnder21Property, value); }
		}
		public Boolean IsOutdoorsOkay
		{
			get { return (Boolean)GetValue(IsOutdoorsOkayProperty); }
			set { SetValue(IsOutdoorsOkayProperty, value); }
		}
		public Boolean IsHomeboundOnly
		{
			get { return (Boolean)GetValue(IsHomeboundOnlyProperty); }
			set { SetValue(IsHomeboundOnlyProperty, value); }
		}
		public Boolean HasCurrentSuggestion
		{
			get { return (Boolean)GetValue(HasCurrentSuggestionProperty); }
			set { SetValue(HasCurrentSuggestionProperty, value); }
		}
		public String CurrentSuggestionText
		{
			get { return (String)GetValue(CurrentSuggestionTextProperty); }
			set { SetValue(CurrentSuggestionTextProperty, value); }
		}
		public Boolean DoesCurrentSuggestionMatchCriteria
		{
			get { return (Boolean)GetValue(DoesCurrentSuggestionMatchCriteriaProperty); }
			set { SetValue(DoesCurrentSuggestionMatchCriteriaProperty, value); }
		}
		public Int32 TotalRemainingSuggestions
		{
			get { return (Int32)GetValue(TotalRemainingSuggestionsProperty); }
			set { SetValue(TotalRemainingSuggestionsProperty, value); }
		}
		public Int32 RemainingSuggestionsMatchingCriteria
		{
			get { return (Int32)GetValue(RemainingSuggestionsMatchingCriteriaProperty); }
			set { SetValue(RemainingSuggestionsMatchingCriteriaProperty, value); }
		}
		public Boolean HasPreviousSuggestions
		{
			get { return (Boolean)GetValue(HasPreviousSuggestionsProperty); }
			set { SetValue(HasPreviousSuggestionsProperty, value); }
		}
		public Boolean HasNextSuggestion
		{
			get { return (Boolean)GetValue(HasNextSuggestionProperty); }
			set { SetValue(HasNextSuggestionProperty, value); }
		}

		readonly ObservableCollection<Suggestion> _previousSuggestions = new ObservableCollection<Suggestion>(); // All of the already generated suggestions before CurrentSuggestion
		readonly ObservableCollection<Suggestion> _nextSuggestions = new ObservableCollection<Suggestion>(); // All of the already generated suggestions after CurrentSuggestion
		BasicCommand _modifyDataCommand;
		BasicCommand _showPreviousSuggestionCommand;
		BasicCommand _showNextSuggestionCommand;

		Suggestion _currentSuggestion;
		Boolean _isInternallySyncingProperties;

		public static readonly DependencyProperty AreFiltersOnProperty = DependencyProperty.Register("AreFiltersOn", typeof(Boolean), typeof(MainWindowViewModel),
			new PropertyMetadata { PropertyChangedCallback = OnCriteriaChanged });
		public static readonly DependencyProperty IsFreeProperty = DependencyProperty.Register("IsFree", typeof(Boolean), typeof(MainWindowViewModel),
			new PropertyMetadata { PropertyChangedCallback = OnCriteriaChanged });
		public static readonly DependencyProperty IsUnder21Property = DependencyProperty.Register("IsUnder21", typeof(Boolean), typeof(MainWindowViewModel),
			new PropertyMetadata { PropertyChangedCallback = OnCriteriaChanged });
		public static readonly DependencyProperty IsOutdoorsOkayProperty = DependencyProperty.Register("IsOutdoorsOkay", typeof(Boolean), typeof(MainWindowViewModel),
			new PropertyMetadata { PropertyChangedCallback = OnCriteriaChanged });
		public static readonly DependencyProperty IsHomeboundOnlyProperty = DependencyProperty.Register("IsHomeboundOnly", typeof(Boolean), typeof(MainWindowViewModel),
			new PropertyMetadata { PropertyChangedCallback = OnCriteriaChanged });
		public static readonly DependencyProperty HasCurrentSuggestionProperty = DependencyProperty.Register("HasCurrentSuggestion", typeof(Boolean), typeof(MainWindowViewModel));
		public static readonly DependencyProperty CurrentSuggestionTextProperty = DependencyProperty.Register("CurrentSuggestionText", typeof(String), typeof(MainWindowViewModel));
		public static readonly DependencyProperty DoesCurrentSuggestionMatchCriteriaProperty = DependencyProperty.Register("DoesCurrentSuggestionMatchCriteria", typeof(Boolean), typeof(MainWindowViewModel));
		public static readonly DependencyProperty TotalRemainingSuggestionsProperty = DependencyProperty.Register("TotalRemainingSuggestions", typeof(Int32), typeof(MainWindowViewModel));
		public static readonly DependencyProperty RemainingSuggestionsMatchingCriteriaProperty = DependencyProperty.Register("RemainingSuggestionsMatchingCriteria", typeof(Int32), typeof(MainWindowViewModel));
		public static readonly DependencyProperty HasPreviousSuggestionsProperty = DependencyProperty.Register("HasPreviousSuggestions", typeof(Boolean), typeof(MainWindowViewModel));
		public static readonly DependencyProperty HasNextSuggestionProperty = DependencyProperty.Register("HasNextSuggestion", typeof(Boolean), typeof(MainWindowViewModel));
	}
}
