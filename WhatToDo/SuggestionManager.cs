﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Xml.Linq;

namespace WhatToDo
{
	class SuggestionManager
	{
		class SuggestionComparer : IEqualityComparer<Suggestion>
		{
			public Boolean Equals(Suggestion a, Suggestion b)
			{
				return (a.IsFree == b.IsFree && a.IsUnder21 == b.IsUnder21 && a.IsOutdoors == b.IsOutdoors && a.IsHomebound == b.IsHomebound &&
					a.Text.Equals(b.Text, StringComparison.InvariantCultureIgnoreCase));
			}

			public Int32 GetHashCode(Suggestion suggestion)
			{
				return suggestion.Text.GetHashCode();
			}
		}

		static SuggestionManager()
		{
			// Create if it doesn't exist
			if (!File.Exists(Constants.SuggestionsXmlFilename))
			{
				Save(Enumerable.Empty<Suggestion>());
				return;
			}

			// Load
			XDocument xml = XDocument.Load(Constants.SuggestionsXmlFilename);

			if (Versioning.UpdateSuggestionsXml(xml.Root))
			{
				xml.Save(Constants.SuggestionsXmlFilename);
			}

			foreach (XElement suggestion in xml.Root.Elements())
			{
				_suggestions.Add(Suggestion.Parse(suggestion));
			}
		}

		public static Suggestion GetNewSuggestion(SuggestionCriteria criteria)
		{
			return GetValidSuggestions(criteria).OrderBy(suggestion => _random.Next()).FirstOrDefault();
		}

		public static Int32 GetRemainingSuggestionsMatchingCriteria(SuggestionCriteria criteria)
		{
			return GetValidSuggestions(criteria).Count();
		}

		public static Int32 GetRemainingSuggestionsTotal(IEnumerable<Suggestion> suggestionsAlreadyGiven)
		{
			return _suggestions.Except(suggestionsAlreadyGiven).Count();
		}

		public static ObservableCollection<Suggestion> GetAllSuggestions()
		{
			return new ObservableCollection<Suggestion>(_suggestions.Select(suggestion => suggestion.Clone()));
		}

		public static void Save(IEnumerable<Suggestion> suggestions)
		{
			XDocument doc = new XDocument();
			XElement xml = new XElement("suggestions");
			doc.Add(xml);

			_suggestions.Clear();
			foreach (Suggestion suggestion in suggestions)
			{
				xml.Add(suggestion.ToXml());
				_suggestions.Add(suggestion);
			}

			doc.Save(Constants.SuggestionsXmlFilename);

			Saved?.Invoke(null, new EventArgs());
		}

		public static event EventHandler Saved;

		static IEnumerable<Suggestion> GetValidSuggestions(SuggestionCriteria criteria)
		{
			IEnumerable<Suggestion> potentialSuggestions = _suggestions.Where(suggestion => suggestion.MatchesCriteria(criteria));
			if (criteria != null)
			{
				potentialSuggestions = potentialSuggestions.Except(criteria.SuggestionsAlreadyGiven, _comparer);
			}
			return potentialSuggestions;
		}

		static readonly Random _random = new Random();
		static readonly List<Suggestion> _suggestions = new List<Suggestion>();
		static readonly SuggestionComparer _comparer = new SuggestionComparer();
	}
}
