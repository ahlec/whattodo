﻿using System;

namespace WhatToDo
{
	public static class Constants
	{
		public const String SuggestionsXmlFilename = "suggestions.xml";
		public const Int32 Version = 1;
		public const Int32 XmlVersion = 1;

		public static readonly String ApplicationName = $"What To Do? v{Version}";
	}
}
