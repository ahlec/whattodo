﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WhatToDo.Converters
{
	class InverseBooleanToVisibilityConverter : IValueConverter
	{
		public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
		{
			if (((Boolean)value))
			{
				return Visibility.Collapsed;
			}
			return Visibility.Visible;
		}

		public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
