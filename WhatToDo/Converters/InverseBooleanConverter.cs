﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WhatToDo.Converters
{
	class InverseBooleanConverter : IValueConverter
	{
		public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
		{
			return !((Boolean)value);
		}

		public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
